import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpense = 50000.0;
    double productExpense = 70000.0;

    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }

    //getter and setter
    public String getName() {
        return customer.name;
    }


    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    
    //phương thức khác
    public double getTotalExpense() {
        return this.serviceExpense + this.productExpense;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", serviceExpense=" + serviceExpense
                + ", productExpense=" + productExpense + ", Total= " + getTotalExpense() + "]";
    }

    
}
