import java.util.Date;
import java.text.SimpleDateFormat;  

public class App {
    public static void main(String[] args) throws Exception {
        // khai báo đối tượng Customer
        Customer customer1 = new Customer("Nguyen Van A");
        Customer customer2 = new Customer("Tran Thi B");

        System.out.println("Customer 1");
        System.out.println(customer1.toString());
        System.out.println("Customer 2");
        System.out.println(customer2.toString());

        // khai báo đối tượng Visit
        String sDate1="2023/12/03"; 
        Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);  
        Visit visit1 = new Visit (customer1, date1);
        System.out.println("Visit 1");
        System.out.println(visit1.toString());
    }
}
