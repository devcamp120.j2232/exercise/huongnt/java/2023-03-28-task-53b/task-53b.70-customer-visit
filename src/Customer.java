import java.util.concurrent.FutureTask;

public class Customer {
    String name;
    boolean member = false;
    String memberType;

    //khởi tạo phương thức
    public Customer(String name) {
        this.name = name;
    }

    //getter
    public String getName() {
        return name;
    }

    public boolean isMember() {
        return member;
    }

    public String getMemberType() {
        return memberType;
    }

    //setter
    public void setMember(boolean member) {
        this.member = member;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    //in ra console
    @Override
    public String toString() {
        return "Customer [name=" + name + ", member=" + member + ", memberType=" + memberType + "]";
    }

    
    
    
    
}
